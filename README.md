**Campaign API**

API para cadastro de campanhas

---
O sistema possui as seguintes regras implementadas:

* As campanhas deverão ser cadastradas de forma que o serviço retorne essas campanhas
 seguindo a estrutura abaixo:
 
> * Nome Da Campanha; <br>
> * ID do Time do Coração;<br>
> * Data de Vigência;
* O Sistema não deverá retornar campanhas que estão com a data de vigência vencidas;
* No cadastramento de uma nova campanha, deve-se verificar se já existe uma campanha ativa
  para aquele período (vigência), caso exista uma campanha ou N campanhas associadas
  naquele período, o sistema deverá somar um dia no término da vigência de cada campanha
  já existente. Caso a data final da vigência seja igual a outra campanha, deverá ser acrescido
  um dia a mais de forma que as campanhas não tenham a mesma data de término de vigência.
  Por fim, efetuar o cadastramento da nova campanha:
 
``` 
Exemplo:
▪ Campanha 1 : inicio dia 01/10/2017 a 03/10/2017;
▪ Campanha 2: inicio dia 01/10/2017 a 02/10/2017;
▪ Cadastrando Campanha 3: inicio 01/10/2017 a 03/10/2017;
▪ -> Sistema:
        • Campanha 2 : 01/10/2017 a 03/10/2017 (porém a data bate com a campanha 1 e a 3, somando mais 1 dia)
            o Campanha 2 : 01/10/2017 a 04/10/2017
        • Campanha 1: 01/10/2017 a 04/10/2017 (bate com a data da campanha 2, somando mais 1 dia)
            o Campanha 1: 01/10/2017 a 05/10/2017
        • Incluindo campanha 3 : 01/10/2017 a 03/10/2017
```
* As campanhas deveram ser controladas por um ID único
* No caso de uma nas campanhas já existentes, o sistema deverá ser capaz de fornecer recursos
  para avisar outros sistemas que houve alteração nas campanhas existentes
* Neste exercício, deve-se contemplar a API que faz a associação entre o Cliente e as
   Campanhas. Essa API é utilizada pelo próximo exercício. O Candidato deve analisar a melhor
   forma e quais os tipos de atributos que devem ser utilizados nessa associação.
---

## Requisitos 
• [Java 1.8](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html) 
<br> • [Docker Componse](https://docs.docker.com/compose/install/#prerequisites)
<br>

---
## Rodando o ambiente com banco de dados em memória

1. Execute a aplicação informando o profile test **spring.profiles.active=test** 

---
## Rodando o ambiente local

1. Rode o banco Mysql em docker usando o camando **docker-compose up**
2. Execute o migration através do flyway para criar a estrutura na base usando o comando **gradle flywayMigrate**
3. Execute a aplicação informando o profile local **spring.profiles.active=local** 
 
---
## Documentação do serviço

Com a aplicação UP basta acessar o endereço http://localhost:8080/swagger-ui.html para acessar
a documentação dos serviços disponibilizados.

---

## Tecnologias utilizadas

* Spring Boot
* Spring Web
* Spring Data JPA
* Spring Cloud Sleuth
* Swagger
* Lombok
* H2 Database
* Flyway
* Mysql in Docker
* OpenFeign

---

## Este serviço em conjunto com mais 2 contempla o projeto como um todo 

#### 1 - Fanatical Partner API - https://bitbucket.org/gabriel-avaliacao/fanatical-partner-api.git
#### 2 - Campaign API - https://bitbucket.org/gabriel-avaliacao/campaign-api.git
#### 3 - Service Discovery API - https://bitbucket.org/gabriel-avaliacao/service-discovery-api.git

---

#### Observações:
Para o perfeito funcionamento da API, verifique:

1 - Banco de dados esteja UP. **docker-compose up -d**
<br>
2 - Subir serviço **Service Discovery API**
<br>
3 - Serviço do **Rabbitmq** esteja UP: A API Service Discovery possui um docker-compose, subir ele também.
<br>
4 - Subir o serviço **Fanatical Partner API**   

---
