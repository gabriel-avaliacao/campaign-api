package com.example.campaignapi.campaign;

import com.example.campaignapi.amqp.publisher.CampaignChangedPublisher;
import com.example.campaignapi.controller.request.CampaignCreationRequest;
import com.example.campaignapi.controller.request.CampaignUpdateRequest;
import com.example.campaignapi.controller.response.CampaignResponse;
import com.example.campaignapi.controller.response.DefaultErrorResponse;
import com.example.campaignapi.domain.CampaignEntity;
import com.example.campaignapi.domain.UserCampaignEntity;
import com.example.campaignapi.domain.UserCampaignEntityKey;
import com.example.campaignapi.exceptions.CampaignBusinessException;
import com.example.campaignapi.exceptions.ValidatePeriodException;
import com.example.campaignapi.repository.CampaignRepository;
import com.example.campaignapi.repository.UserCampaignRepository;
import com.example.campaignapi.service.impl.CampaignServiceImpl;
import com.example.campaignapi.utils.MessageDefinitionUtils;
import com.example.campaignapi.utils.ValidatePeriodUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles(profiles = "test")
class CampaignServiceTest {

    public static final LocalDate TODAY = LocalDate.now();
    public static final String START_PERIOD_VALID = TODAY.format(ValidatePeriodUtils.DATE_FORMAT);
    public static final LocalDate START_PERIOD_VALID_LOCALDATE = LocalDate.parse(START_PERIOD_VALID, ValidatePeriodUtils.DATE_FORMAT);
    public static final String END_PERIOD_VALID = TODAY.plusDays(4L).format(ValidatePeriodUtils.DATE_FORMAT);
    public static final LocalDate END_PERIOD_VALID_LOCALDATE = LocalDate.parse(END_PERIOD_VALID, ValidatePeriodUtils.DATE_FORMAT);
    public static final long TEAM_ID = 1L;
    public static final String NAME_CAMPANHA_TEST = "Campanha 1";
    public static final Boolean ACTIVE_STATUS_VALID = Boolean.TRUE;
    public static final long CAMPAIGN_ID = 1L;
    public static final String CUSTOMER_EMAIL = "teste@teste.com";

    @InjectMocks
    private CampaignServiceImpl campaignService;

    @Mock
    private CampaignRepository campaignRepository;

    @Mock
    private UserCampaignRepository userCampaignRepository;

    @Mock
    private CampaignChangedPublisher campaignChangedPublisher;

    @Test
    void testCreateSuccess(){
        final CampaignEntity entity = mockCampaignEntity();
        final CampaignCreationRequest request = mockCampaignRequest();
        when(campaignRepository.save(any())).thenReturn(entity);
        doNothing().when(campaignChangedPublisher).publishCampaignChanged(any(),any());
        CampaignResponse campaignResponse = campaignService.create(request);

        assertEquals(entity.getName(), campaignResponse.getName());
        assertEquals(entity.getTeamId(), campaignResponse.getTeamId());
        assertEquals(entity.getStartPeriod(), campaignResponse.getStartPeriod());
        assertEquals(entity.getEndPeriod(), campaignResponse.getEndPeriod());
        assertEquals(entity.isActive(), campaignResponse.isActive());
    }

    @Test
    void testCreateWithStartPeriodAfterTodaySuccess(){
        CampaignEntity entity = mockCampaignEntity();
        entity.setActive(Boolean.FALSE);
        CampaignCreationRequest request = mockCampaignRequest();
        request.setStartPeriod(TODAY.plusDays(1).format(ValidatePeriodUtils.DATE_FORMAT));
        when(campaignRepository.save(any())).thenReturn(entity);
        doNothing().when(campaignChangedPublisher).publishCampaignChanged(any(),any());
        CampaignResponse campaignResponse = campaignService.create(request);
        assertEquals(entity.isActive(), campaignResponse.isActive());
    }

    @Test
    void testCreateWithInvalidDateStartAndEndPeriod(){
        DefaultErrorResponse expectedErrorResponse = MessageDefinitionUtils.ERROR_VALIDATE_DATE.getDefaultErrorResponse();

        DefaultErrorResponse errorResponseStartPeriod = getDefaultErrorResponseStartPeriod();
        DefaultErrorResponse errorResponseEndPeriod = getDefaultErrorResponseEndPeriod();

        assertEquals(expectedErrorResponse.getCode(),errorResponseStartPeriod.getCode());
        assertEquals(expectedErrorResponse.getDescription(),errorResponseStartPeriod.getDescription());

        assertEquals(expectedErrorResponse.getCode(),errorResponseEndPeriod.getCode());
        assertEquals(expectedErrorResponse.getDescription(),errorResponseEndPeriod.getDescription());
    }

    @Test
    void testCreateWithStartPeriodAfterEndPeriod(){
        DefaultErrorResponse expectedErrorResponse = MessageDefinitionUtils.ERROR_START_DATE_AFTER_END_DATE.getDefaultErrorResponse();

        CampaignCreationRequest request = mockCampaignRequest();
        String startPeriodAfterEndPeriod = END_PERIOD_VALID_LOCALDATE.plusDays(12L).format(ValidatePeriodUtils.DATE_FORMAT);
        request.setStartPeriod(startPeriodAfterEndPeriod);

        DefaultErrorResponse errorResponse =
                Assertions.assertThrows(ValidatePeriodException.class, () -> campaignService.create(request))
                        .getMessageDefinition().getDefaultErrorResponse();

        assertEquals(expectedErrorResponse.getCode(),errorResponse.getCode());
        assertEquals(expectedErrorResponse.getDescription(),errorResponse.getDescription());
    }

    @Test
    void testCreateWithEndPeriodBeforeToday(){
        DefaultErrorResponse expectedErrorResponse = MessageDefinitionUtils.ERROR_END_DATE_BEFORE_TODAY.getDefaultErrorResponse();

        CampaignCreationRequest request = mockCampaignRequest();
        String startPeriod = TODAY.minusDays(5L).format(ValidatePeriodUtils.DATE_FORMAT);
        String endPeriod = TODAY.minusDays(1L).format(ValidatePeriodUtils.DATE_FORMAT);
        request.setStartPeriod(startPeriod);
        request.setEndPeriod(endPeriod);

        DefaultErrorResponse errorResponse =
                Assertions.assertThrows(ValidatePeriodException.class, () -> campaignService.create(request))
                        .getMessageDefinition().getDefaultErrorResponse();

        assertEquals(expectedErrorResponse.getCode(),errorResponse.getCode());
        assertEquals(expectedErrorResponse.getDescription(),errorResponse.getDescription());
    }

    @Test
    void testfindAllCampaignActivesSuccess(){
        CampaignEntity entity = mockCampaignEntity();
        List<CampaignEntity> campaignEntities = Arrays.asList(entity);
        when(campaignRepository.findByActiveTrue()).thenReturn(campaignEntities);
        List<CampaignResponse> campaignResponses = campaignService.findAllActives();

        assertNotNull(campaignResponses);
        assertEquals(campaignEntities.size(), campaignResponses.size());
        assertEquals(entity.getName(), campaignResponses.get(0).getName());
        assertEquals(entity.getTeamId(), campaignResponses.get(0).getTeamId());
        assertEquals(entity.getStartPeriod(), campaignResponses.get(0).getStartPeriod());
        assertEquals(entity.getEndPeriod(), campaignResponses.get(0).getEndPeriod());
        assertEquals(entity.isActive(), campaignResponses.get(0).isActive());
    }

    @Test
    void testfindCampaignActivesByTeamSuccess(){
        CampaignEntity entity = mockCampaignEntity();
        List<CampaignEntity> campaignEntities = Arrays.asList(entity);
        when(campaignRepository.findByActiveTrueAndTeamId(TEAM_ID)).thenReturn(campaignEntities);
        List<CampaignResponse> campaignResponses = campaignService.findActivesByTeam(TEAM_ID);

        assertNotNull(campaignResponses);
        assertEquals(campaignEntities.size(), campaignResponses.size());
        assertEquals(entity.getName(), campaignResponses.get(0).getName());
        assertEquals(entity.getTeamId(), campaignResponses.get(0).getTeamId());
        assertEquals(entity.getStartPeriod(), campaignResponses.get(0).getStartPeriod());
        assertEquals(entity.getEndPeriod(), campaignResponses.get(0).getEndPeriod());
        assertEquals(entity.isActive(), campaignResponses.get(0).isActive());
    }

    @Test
    void testNotFoundInfindAllCampaignActives(){
        when(campaignRepository.findByActiveTrue()).thenReturn(Collections.emptyList());
        List<CampaignResponse> campaignResponses = campaignService.findAllActives();

        assertNotNull(campaignResponses);
        assertEquals(Collections.emptyList().size(), campaignResponses.size());
    }

    @Test
    void testDeleteSuccess(){
        when(campaignRepository.findById(CAMPAIGN_ID)).thenReturn(Optional.of(mockCampaignEntity()));
        doNothing().when(userCampaignRepository).deleteByCampaignId(CAMPAIGN_ID);
        doNothing().when(campaignRepository).deleteById(CAMPAIGN_ID);
        campaignService.delete(CAMPAIGN_ID);
    }

    @Test
    void testDeleteCampaignNotFound(){
        DefaultErrorResponse expectedErrorResponse = MessageDefinitionUtils.ERROR_CAMPAIGN_NOT_FOUND.getDefaultErrorResponse();

        when(campaignRepository.findById(CAMPAIGN_ID)).thenReturn(Optional.empty());
        doNothing().when(userCampaignRepository).deleteByCampaignId(CAMPAIGN_ID);
        doNothing().when(campaignChangedPublisher).publishCampaignChanged(any(),any());
        DefaultErrorResponse errorResponse = Assertions.assertThrows(CampaignBusinessException.class, () -> campaignService.delete(CAMPAIGN_ID))
                .getMessageDefinition().getDefaultErrorResponse();

        assertEquals(expectedErrorResponse.getCode(),errorResponse.getCode());
        assertEquals(expectedErrorResponse.getDescription(),errorResponse.getDescription());
    }

    @Test
    void testUpdateSuccess(){
        CampaignUpdateRequest request = mockCampaignUpdateRequest();
        CampaignEntity entity = mockCampaignEntity();

        when(campaignRepository.findById(CAMPAIGN_ID)).thenReturn(Optional.of(entity));
        when(campaignRepository.save(entity)).thenReturn(entity);
        doNothing().when(campaignChangedPublisher).publishCampaignChanged(any(),any());
        CampaignResponse campaignResponse = campaignService.update(request, CAMPAIGN_ID);

        assertEquals(entity.getName(), campaignResponse.getName());
        assertEquals(entity.getTeamId(), campaignResponse.getTeamId());
        assertEquals(entity.getStartPeriod(), campaignResponse.getStartPeriod());
        assertEquals(entity.getEndPeriod(), campaignResponse.getEndPeriod());
        assertEquals(entity.isActive(), campaignResponse.isActive());
        assertNotNull(campaignResponse.getUpdateDate());
    }

    @Test
    void testFindById(){
        CampaignEntity entity = mockCampaignEntity();
        when(campaignRepository.findById(CAMPAIGN_ID)).thenReturn(Optional.of(entity));
        CampaignResponse campaignResponse = campaignService.findById(CAMPAIGN_ID);

        assertNotNull(campaignResponse);
        assertEquals(entity.getId(),campaignResponse.getId());
        assertEquals(entity.getName(),campaignResponse.getName());
        assertEquals(entity.getTeamId(),campaignResponse.getTeamId());
        assertEquals(entity.getStartPeriod(),campaignResponse.getStartPeriod());
        assertEquals(entity.getEndPeriod(),campaignResponse.getEndPeriod());
    }

    @Test
    void testRelateCustomerWithCampaignSuccess(){
        when(campaignRepository.findById(CAMPAIGN_ID)).thenReturn(Optional.of(mockCampaignEntity()));
        when(userCampaignRepository.save(mockUserCampaignEntity())).thenReturn(any());
        campaignService.relateCustomer(CAMPAIGN_ID, CUSTOMER_EMAIL);
    }

    @Test
    void testRelateCustomerWithDataIntegrityViolationException(){
        when(campaignRepository.findById(CAMPAIGN_ID)).thenReturn(Optional.of(mockCampaignEntity()));
        when(userCampaignRepository.save(mockUserCampaignEntity())).thenThrow(DataIntegrityViolationException.class);
        Assertions.assertThrows(DataIntegrityViolationException.class,
                () -> campaignService.relateCustomer(CAMPAIGN_ID, CUSTOMER_EMAIL));
    }

    private DefaultErrorResponse getDefaultErrorResponseStartPeriod() {
        CampaignCreationRequest request = mockCampaignRequest();
        request.setStartPeriod("01/20/2021");

        return Assertions.assertThrows(ValidatePeriodException.class, () -> campaignService.create(request))
                .getMessageDefinition().getDefaultErrorResponse();
    }

    private DefaultErrorResponse getDefaultErrorResponseEndPeriod() {
        CampaignCreationRequest request = mockCampaignRequest();
        request.setEndPeriod("34/12/2021");

        return Assertions.assertThrows(ValidatePeriodException.class, () -> campaignService.create(request))
                .getMessageDefinition().getDefaultErrorResponse();
    }

    private CampaignEntity mockCampaignEntity() {
        CampaignEntity entity = new CampaignEntity();
        entity.setId(CAMPAIGN_ID);
        entity.setName(NAME_CAMPANHA_TEST);
        entity.setTeamId(TEAM_ID);
        entity.setActive(ACTIVE_STATUS_VALID);
        entity.setStartPeriod(START_PERIOD_VALID_LOCALDATE);
        entity.setEndPeriod(END_PERIOD_VALID_LOCALDATE);
        return entity;
    }

    private CampaignCreationRequest mockCampaignRequest(){
        return CampaignCreationRequest
                .builder()
                .name(NAME_CAMPANHA_TEST)
                .teamId(TEAM_ID)
                .startPeriod(START_PERIOD_VALID)
                .endPeriod(END_PERIOD_VALID)
                .build();
    }

    private CampaignUpdateRequest mockCampaignUpdateRequest(){
        return CampaignUpdateRequest
                .builder()
                .name(NAME_CAMPANHA_TEST)
                .teamId(TEAM_ID)
                .startPeriod(START_PERIOD_VALID)
                .endPeriod(END_PERIOD_VALID)
                .build();
    }

    private UserCampaignEntity mockUserCampaignEntity(){
        UserCampaignEntity entity = new UserCampaignEntity();
        entity.setCampaign(mockCampaignEntity());
        entity.setComposeId(mockUserCampaignEntityKey());
        return entity;
    }

    private UserCampaignEntityKey mockUserCampaignEntityKey(){
        UserCampaignEntityKey entityKey = new UserCampaignEntityKey();
        entityKey.setEmail(CUSTOMER_EMAIL);
        return entityKey;
    }
}
