package com.example.campaignapi.service;

import com.example.campaignapi.controller.request.CampaignCreationRequest;
import com.example.campaignapi.controller.request.CampaignUpdateRequest;
import com.example.campaignapi.controller.response.CampaignResponse;
import com.example.campaignapi.domain.CampaignEntity;

import java.util.List;

public interface CampaignService {

    CampaignResponse create(CampaignCreationRequest request);

    List<CampaignResponse> findAllActives();

    List<CampaignEntity> findAllActiveEntity();

    void delete(Long id);

    void saveEntity(CampaignEntity entity);

    CampaignResponse update(CampaignUpdateRequest request, Long campaignId);

    void disabledExpiredCampaign();

    void activeCampaigns();

    CampaignResponse findById(Long id);

    void relateCustomer(Long campaignId, String email);

    List<CampaignResponse> findCampaignByCustomer(String email);

    List<CampaignResponse> findActivesByTeam(Long teamId);
}
