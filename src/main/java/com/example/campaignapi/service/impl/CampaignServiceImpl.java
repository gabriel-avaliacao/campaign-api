package com.example.campaignapi.service.impl;

import com.example.campaignapi.amqp.publisher.CampaignChangedPublisher;
import com.example.campaignapi.controller.request.CampaignCreationRequest;
import com.example.campaignapi.controller.request.CampaignUpdateRequest;
import com.example.campaignapi.controller.response.CampaignResponse;
import com.example.campaignapi.domain.CampaignEntity;
import com.example.campaignapi.domain.UserCampaignEntity;
import com.example.campaignapi.domain.UserCampaignEntityKey;
import com.example.campaignapi.enums.CampaignStatus;
import com.example.campaignapi.exceptions.CampaignBusinessException;
import com.example.campaignapi.exceptions.ValidatePeriodException;
import com.example.campaignapi.repository.CampaignRepository;
import com.example.campaignapi.repository.UserCampaignRepository;
import com.example.campaignapi.service.CampaignService;
import com.example.campaignapi.utils.MessageDefinitionUtils;
import com.example.campaignapi.utils.ValidatePeriodUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class CampaignServiceImpl implements CampaignService {

    private final CampaignRepository campaignRepository;

    private final UserCampaignRepository userCampaignRepository;

    private final CampaignChangedPublisher campaignChangedPublisher;

    /**
     * Salva uma nova campanha na base de dados com base no parâmetro informado.
     * @param campaignCreationRequest Objeto de requisição de uma campanha
     * @return CampaignResponse representação de resposta de uma campanha
     */
    @Transactional
    public CampaignResponse create(CampaignCreationRequest campaignCreationRequest) {
        log.info("M=create, status=start, campanha={}",campaignCreationRequest);
        CampaignEntity entity = convertToSaveEntity(campaignCreationRequest);
        CampaignEntity campaignEntity = validateEntity(entity);
        verifyValidityPeriod(campaignEntity);
        CampaignResponse response = converToResponse(campaignRepository.save(campaignEntity));
        postChange(campaignEntity.getId(), CampaignStatus.CREATED);
        log.info("M=create, status=end, campanha={}",campaignCreationRequest);
        return response;
    }

    private CampaignEntity validateEntity(CampaignEntity campaignEntity) {
        log.info("M=validateEntity, status=start, campanha={}",campaignEntity);
        verifyPeriods(campaignEntity);
        fillActiveByPeriod(campaignEntity);
        log.info("M=validateEntity, status=start, campanha={}",campaignEntity);
        return campaignEntity;
    }

    private void verifyValidityPeriod(CampaignEntity campaignEntity) {
        log.info("M=verifyValidityPeriod, status=start, campanha={}",campaignEntity);
        List<CampaignEntity> campaignEntities = findAllActiveEntity();
        log.info("M=verifyValidityPeriod, Total de campanhas ativas={}",campaignEntities.size());
        // Se essa regra valer para quando um objeto for atualizado
        // descomentar a linha abaixo e chamar esse método quando for atualizar
        //campaignEntities.remove(campaignEntity);
        long count;
        boolean hasChange = false;
        do{
            count = campaignEntities.stream().filter(c -> c.getEndPeriod().equals(campaignEntity.getEndPeriod())).count();
            if(count != 0){
                hasChange = true;
                campaignEntities.forEach(c -> c.setEndPeriod(c.getEndPeriod().plusDays(1L)));
            }
        } while (count != 0);
        if(hasChange){
            saveModifiedCampaign(campaignEntities);
        }
        log.info("M=verifyValidityPeriod, status=end, campanha={}",campaignEntity);
    }

    private void saveModifiedCampaign(List<CampaignEntity> campaignEntities) {
        log.info("M=saveModifiedCampaign, status=start, total de campanhas que serão atualizadas={}",campaignEntities.size());
        campaignEntities.forEach(this::saveEntity);
        log.info("M=saveModifiedCampaign, status=end.");
    }

    private CampaignEntity convertToSaveEntity(CampaignCreationRequest campaignCreationRequest) {
        CampaignEntity entity = new CampaignEntity();
        entity.setName(campaignCreationRequest.getName());
        entity.setTeamId(campaignCreationRequest.getTeamId());
        entity.setStartPeriod(ValidatePeriodUtils.isValid(campaignCreationRequest.getStartPeriod()));
        entity.setEndPeriod(ValidatePeriodUtils.isValid(campaignCreationRequest.getEndPeriod()));
        entity.setCreatedDate(LocalDate.now());
        return entity;
    }

    private CampaignEntity fillEntityToUpdateByRequest(CampaignUpdateRequest campaignUpdateRequest, CampaignEntity entity ) {
        if(isValidObjectValue(campaignUpdateRequest.getName())){
            entity.setName(campaignUpdateRequest.getName());
        }
        if(isValidObjectValue(campaignUpdateRequest.getTeamId())){
            entity.setTeamId(campaignUpdateRequest.getTeamId());
        }
        if(isValidObjectValue(campaignUpdateRequest.getStartPeriod())){
            entity.setStartPeriod(ValidatePeriodUtils.isValid(campaignUpdateRequest.getStartPeriod()));
        }
        if(isValidObjectValue(campaignUpdateRequest.getEndPeriod())){
            entity.setEndPeriod(ValidatePeriodUtils.isValid(campaignUpdateRequest.getEndPeriod()));
        }
        entity.setUpdateDate(LocalDate.now());
        return entity;
    }

    private boolean isValidObjectValue(Object value) {
        return value != null && !ObjectUtils.isEmpty(value);
    }

    private void verifyPeriods(CampaignEntity campaignEntity) {
        log.info("M=verifyPeriods, status=start, campanha={}",campaignEntity);
        if(campaignEntity.getStartPeriod().isAfter(campaignEntity.getEndPeriod())){
            log.info("M=verifyPeriods, status=error, exception={}, campanha={}",
                    MessageDefinitionUtils.ERROR_START_DATE_AFTER_END_DATE.getDescription(),campaignEntity);
            throw new ValidatePeriodException(MessageDefinitionUtils.ERROR_START_DATE_AFTER_END_DATE);
        }
        if(campaignEntity.getEndPeriod().isBefore(LocalDate.now())){
            log.info("M=verifyPeriods, status=error, exception={}, campanha={}",
                    MessageDefinitionUtils.ERROR_END_DATE_BEFORE_TODAY.getDescription(),campaignEntity);
            throw new ValidatePeriodException(MessageDefinitionUtils.ERROR_END_DATE_BEFORE_TODAY);
        }
        log.info("M=verifyPeriods, status=end, campanha={}",campaignEntity);
    }

    private void fillActiveByPeriod(CampaignEntity campaignEntity) {
        if(campaignEntity.getStartPeriod().isAfter(LocalDate.now()))
            campaignEntity.setActive(Boolean.FALSE);
        else
            campaignEntity.setActive(Boolean.TRUE);
    }

    /**
     * Busca todas as campanhas que estão ativas e as converte em objetos CampaignResponse
     * @return List<CampaignResponse> Lista resposta de representações da entidade campanha
     */
    @Override
    public List<CampaignResponse> findAllActives() {
        log.info("M=findAllActives, status=start, busca todas as camanhas ativas na base de dados.");
        List<CampaignEntity> campaignEntities = findAllActiveEntity();
        List<CampaignResponse> response = campaignEntities.stream()
                .map(this::converToResponse)
                .collect(Collectors.toList());
        log.info("M=findAllActives, status=end. total de campanhas={}",response.size());
        return response;
    }

    /**
     * Busca todas as campanhas que estão ativas
     * @return List<CampaignEntity> Lista de representações da entidade campanha
     */
    @Override
    public List<CampaignEntity> findAllActiveEntity() {
        return campaignRepository.findByActiveTrue();
    }

    /**
     * Deleta uma campanha com base no seu identificador.
     * Caso o identificador não esteja cadastrado na base de dados uma exception do tipo
     * CampaignBusinessException será lançada
     * @param campaignId identificador da campanha
     */
    @Override
    @Transactional
    public void delete(Long campaignId) {
        log.info("M=delete, status=start, deletando campanha na base de dados através do campaignId={}.",campaignId);
        findCompaignById(campaignId);
        deleteRelationshipWithCustomer(campaignId);
        deleteCampaignById(campaignId);
        postChange(campaignId, CampaignStatus.DELETED);
        log.info("M=delete, status=end, campanha com id campanhaId={} apagado.",campaignId);
    }

    private void postChange(Long campaignId, CampaignStatus deleted) {
        campaignChangedPublisher.publishCampaignChanged(campaignId,deleted);
    }

    private void deleteRelationshipWithCustomer(Long campaignId) {
        log.info("M=deleteRelationshipWithCustomer, status=start, verificando se campanha campaignId={} possui vinculo com usuario.",campaignId);
        userCampaignRepository.deleteByCampaignId(campaignId);
        log.info("M=deleteRelationshipWithCustomer, status=end, verificação finalizada.");
    }

    private void deleteCampaignById(Long campaignId) {
        log.info("M=deleteCampaignById, status=start, apagando campanha com id={}.", campaignId);
        campaignRepository.deleteById(campaignId);
        log.info("M=deleteCampaignById, status=end, processo finalizado.");
    }

    /**
     * Salva uma campanha na base de dados
     * @param campaignEntity Representação da entidade campanha
     */
    @Override
    public void saveEntity(CampaignEntity campaignEntity) {
        campaignRepository.save(campaignEntity);
    }

    /**
     * Atualiza uma entidade campanha com base nos parâmetros informados.
      * @param campaignUpdateRequest Representação de um objeto campanha para atualizar
     * @param campaignId Identificador da campanha
     * @return CampaignResponse Objeto de resposta da entidade campanha
     */
    @Override
    @Transactional
    public CampaignResponse update(CampaignUpdateRequest campaignUpdateRequest, Long campaignId) {
        log.info("M=update, status=start, campaignId={}, campanha={}, ",campaignId,campaignUpdateRequest);
        CampaignEntity campaign = findCompaignById(campaignId);
        fillEntityToUpdateByRequest(campaignUpdateRequest,campaign);
        fillActiveByPeriod(campaign);
        CampaignResponse response = converToResponse(campaignRepository.save(campaign));
        postChange(response.getId(), CampaignStatus.UPDATED);
        log.info("M=update, status=end");
        return response;
    }

    private CampaignEntity findCompaignById(Long campaignId) {
        log.info("M=findCompaignById, status=start, buscando campanha na base de dados campaignId={}.",campaignId);
        CampaignEntity response = campaignRepository.findById(campaignId)
                .orElseThrow(() -> new CampaignBusinessException(MessageDefinitionUtils.ERROR_CAMPAIGN_NOT_FOUND));
        log.info("M=findCompaignById, status=end. campanha encontrada, campanha={}",response);
        return response;
    }

    /**
     * Desabilita campanhas com data vigência expirada.
     */
    @Override
    public void disabledExpiredCampaign() {
        log.info("M=disabledExpiredCampaign, status=start.");
        campaignRepository.updateCompaignsByActiveTrueAndEndPeriodBeforeToday(LocalDate.now());
        log.info("M=disabledExpiredCampaign, status=end.");
    }

    /**
     * Habilita campanhas que possuem hoje como data vigente inicial
     */
    @Override
    public void activeCampaigns() {
        log.info("M=activeCampaigns, status=start");
        campaignRepository.updateCompaignsByActiveFalseAndStartPeriodEqualsOrAfterToday(LocalDate.now());
        log.info("M=activeCampaigns, status=end");
    }

    /**
     * Busca uma campanha com base no identificador.
     * @param campaignId identificador da campanha
     * @return CampaignResponse Objeto de de resposta que representa uma entidade campanha
     */
    @Override
    public CampaignResponse findById(Long campaignId) {
        log.info("M=findById, status=start, procurando campanha com id={}",campaignId);
        CampaignResponse response = converToResponse(findCompaignById(campaignId));
        log.info("M=findById, status=end, campanha={} ",response);
        return response;
    }

    /**
     * Relaciona um campanha com um determinado usuário com base no seu email e identificador da campanha
     * @param campaignId identificador da campanha
     * @param email identificador do usuário
     */
    @Override
    public void relateCustomer(Long campaignId, String email) {
        log.info("M=relateCustomer, status=start, vinculando campanhaId={} com usuario={}",campaignId,email);
        if(!isValidObjectValue(email))
            throw new CampaignBusinessException(MessageDefinitionUtils.ERROR_EMAIL_EMPTY_OR_NULL);

        CampaignEntity campaignEntity = findCompaignById(campaignId);
        prepareToSaveUserCampaignEntity(campaignEntity,email);
        log.info("M=relateCustomer, status=end.");
    }

    /**
     * Busca na base de dados campanhas ativas através do identificador do usuário
     * @param email identificador do usuário
     * @return List<CampaignResponse>
     */
    @Override
    public List<CampaignResponse> findCampaignByCustomer(String email) {
        log.info("M=findCampaignByCustomer, status=start, buscando campanhas que possuem vinculo com usuario={}",email);
        List<CampaignEntity> campaignEntities = userCampaignRepository.findByComposeIdEmail(email);
        List<CampaignResponse> response = campaignEntities.stream().map(this::converToResponse).collect(Collectors.toList());
        log.info("M=findCampaignByCustomer, status=end, total de campanhas encontradas, total={}",response.size());
        return response;
    }

    /**
     *  Busca na base de dados campanhas ativas através do identificador do time do coração
     * @param teamId identificador do time do coração
     * @return List<CampaignResponse>
     */
    @Override
    public List<CampaignResponse> findActivesByTeam(Long teamId) {
        log.info("M=findActivesByTeam, status=start, procurando campanhas ativas com base no teamId={}",teamId);
        List<CampaignEntity> campaignEntities = campaignRepository.findByActiveTrueAndTeamId(teamId);
        List<CampaignResponse> response = campaignEntities.stream()
                .map(this::converToResponse)
                .collect(Collectors.toList());
        log.info("M=findActivesByTeam, status=end, foram encontradas um total de {} campanhas ativas para esse teamId={}"
                ,response.size(),teamId);
        return response;
    }

    private void prepareToSaveUserCampaignEntity(CampaignEntity campaignEntity, String email) {
        log.info("M=prepareToSaveUserCampaignEntity, status=start, preparando para vincular campanha={} com o usuario={}",campaignEntity,email);
        UserCampaignEntityKey userCampaignEntityKey = createUserCampaignEntityKey(email);

        UserCampaignEntity userCampaignEntity = createUserCampaignEntity(campaignEntity, userCampaignEntityKey);

        UserCampaignEntity response = userCampaignRepository.save(userCampaignEntity);
        log.info("M=prepareToSaveUserCampaignEntity, status=end, vinculo salvo userCampaign={}",response);
    }

    private UserCampaignEntity createUserCampaignEntity(CampaignEntity campaignEntity, UserCampaignEntityKey userCampaignEntityKey) {
        UserCampaignEntity userCampaignEntity = new UserCampaignEntity();
        userCampaignEntity.setCampaign(campaignEntity);
        userCampaignEntity.setComposeId(userCampaignEntityKey);
        return userCampaignEntity;
    }

    private UserCampaignEntityKey createUserCampaignEntityKey(String email) {
        UserCampaignEntityKey userCampaignEntityKey = new UserCampaignEntityKey();
        userCampaignEntityKey.setEmail(email);
        return userCampaignEntityKey;
    }

    private CampaignResponse converToResponse(CampaignEntity campaignEntity) {
        return CampaignResponse.builder()
                .id(campaignEntity.getId())
                .createdDate(campaignEntity.getCreatedDate())
                .teamId(campaignEntity.getTeamId())
                .name(campaignEntity.getName())
                .startPeriod(campaignEntity.getStartPeriod())
                .endPeriod(campaignEntity.getEndPeriod())
                .updateDate(campaignEntity.getUpdateDate())
                .active(campaignEntity.isActive())
                .build();
    }
}
