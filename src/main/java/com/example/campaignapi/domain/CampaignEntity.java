package com.example.campaignapi.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "campaign")
public class CampaignEntity {

    @Id
    @Column(name = "idt_campaign")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "des_name", nullable = false)
    private String name;

    @Column(name = "idt_team", nullable = false)
    private Long teamId;

    @Column(name = "dat_start_period", nullable = false)
    private LocalDate startPeriod;

    @Column(name = "dat_end_period", nullable = false)
    private LocalDate endPeriod;

    @Column(name = "dat_created")
    private LocalDate createdDate;

    @Column(name = "dat_update")
    private LocalDate updateDate;

    @Column(name = "flg_active")
    private boolean active;

}
