package com.example.campaignapi.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class UserCampaignEntityKey implements Serializable {

    @Column(name = "des_user_email")
    private String email;

    @Column(name = "idt_campaign")
    private Long campaignId;
}
