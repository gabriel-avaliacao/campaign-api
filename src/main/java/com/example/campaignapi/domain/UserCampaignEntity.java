package com.example.campaignapi.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_campaign")
public class UserCampaignEntity {

    @EmbeddedId
    private UserCampaignEntityKey composeId;

    @ManyToOne
    @MapsId("campaignId")
    @JoinColumn(name = "idt_campaign")
    private CampaignEntity campaign;
}
