package com.example.campaignapi.amqp.config;

import com.example.campaignapi.amqp.RabbitmqPropertiesConfig;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CampaignChangedRabbitmqConfig {

    @Autowired
    private RabbitmqPropertiesConfig propertiesConfig;

    @Bean
    public Queue queueCampaignChanged() {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", propertiesConfig.getExchangeCampaignChanged());
        arguments.put("x-dead-letter-routing-key", propertiesConfig.getRoutingKeyCampaignChangedDlq());
        return new Queue(propertiesConfig.getQueueCampaignChanged(), true,false,false, arguments);
    }

    @Bean
    public Queue queueCampaignChangedDlq() {
        return new Queue(propertiesConfig.getQueueCampaignChangedDlq(), true);
    }

    @Bean
    public DirectExchange exchangeCampaignChanged() {
        return new DirectExchange(propertiesConfig.getExchangeCampaignChanged());
    }

    @Bean
    public DirectExchange exchangeCampaignChangedDlq() {
        return new DirectExchange(propertiesConfig.getExchangeCampaignChangedDlq());
    }

    @Bean
    public Binding bindingCampaignChanged() {
        return BindingBuilder.bind(queueCampaignChanged()).to(exchangeCampaignChanged())
                .with(propertiesConfig.getRoutingKeyCampaignChanged());
    }

    @Bean
    public Binding bindingCampaignChangedDlq() {
        return BindingBuilder.bind(queueCampaignChangedDlq()).to(exchangeCampaignChangedDlq())
                .with(propertiesConfig.getRoutingKeyCampaignChangedDlq());
    }

}
