package com.example.campaignapi.amqp.publisher;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class GenericPublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void publishRetry(Object message, String exchange, String routingKey) {
        try{
            log.info("M=publishRetry, republicando mensagem na fila, message={}",message);
            rabbitTemplate.convertAndSend(exchange,routingKey,message);
            log.info("M=publishRetry, mensagem republicada, message={}",message);
        }catch (Exception e){
            log.error("M=publishRetry, Erro ao republicar mensagem na fila, message={}",message);
            throw new AmqpRejectAndDontRequeueException
                    ("Erro ao republicar mensagem na fila="+message.toString());
        }
    }

}
