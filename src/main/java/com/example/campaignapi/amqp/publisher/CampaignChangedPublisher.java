package com.example.campaignapi.amqp.publisher;

import com.example.campaignapi.CampaignMessageDTO;
import com.example.campaignapi.amqp.RabbitmqPropertiesConfig;
import com.example.campaignapi.domain.CampaignEntity;
import com.example.campaignapi.enums.CampaignStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CampaignChangedPublisher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitmqPropertiesConfig fanaticalProperties;

    public void publishCampaignChanged(Long campaignId, CampaignStatus status){
        try{
            log.info("M=publishCampaignChanged, status=start, mensagem enviada com sucesso, campanhaId={}",campaignId);
            CampaignMessageDTO message = CampaignMessageDTO.builder()
                    .campaignId(campaignId).status(status.name())
                    .build();
            rabbitTemplate.convertAndSend(fanaticalProperties.getExchangeCampaignChanged(),
                    fanaticalProperties.getRoutingKeyCampaignChanged(), message);
            log.info("M=publishCampaignChanged, status=end, mensagem enviada com sucesso, mensagem={}",message);
        }catch (Exception e){
            log.info("M=publishCampaignChanged, status=error, erro ao enviar mensagem para exchange={}, routing-key={}"
                    ,fanaticalProperties.getExchangeCampaignChanged(),
                    fanaticalProperties.getRoutingKeyCampaignChanged());
        }
    }
}
