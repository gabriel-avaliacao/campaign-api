package com.example.campaignapi.amqp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "amqp-rabbitmq")
public class RabbitmqPropertiesConfig {

    // PROCESSO NOVO USUARIO
    private String queueCampaignChanged;
    private String exchangeCampaignChanged;
    private String routingKeyCampaignChanged;
    private String queueCampaignChangedDlq;
    private String exchangeCampaignChangedDlq;
    private String routingKeyCampaignChangedDlq;
    private long queueCampaignChangedMaxRetry;
}
