package com.example.campaignapi;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableRabbit
@EnableScheduling
@EnableDiscoveryClient
@SpringBootApplication
public class CampaignApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CampaignApiApplication.class, args);
	}

}
