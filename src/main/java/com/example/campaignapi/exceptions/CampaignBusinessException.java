package com.example.campaignapi.exceptions;

import com.example.campaignapi.utils.MessageDefinitionUtils;
import lombok.Getter;

public class CampaignBusinessException extends RuntimeException {

    @Getter
    private MessageDefinitionUtils messageDefinition;

    public CampaignBusinessException(MessageDefinitionUtils messageDefinition) {
        this.messageDefinition = messageDefinition;
    }
}
