package com.example.campaignapi.exceptions;

import com.example.campaignapi.utils.MessageDefinitionUtils;
import lombok.Getter;

public class ValidatePeriodException extends RuntimeException {

    @Getter
    private MessageDefinitionUtils messageDefinition;

    public ValidatePeriodException(MessageDefinitionUtils messageDefinition) {
        this.messageDefinition = messageDefinition;
    }
}
