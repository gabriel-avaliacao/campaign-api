package com.example.campaignapi.repository;


import com.example.campaignapi.domain.CampaignEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CampaignRepository extends JpaRepository<CampaignEntity, Long> {

    List<CampaignEntity> findByActiveTrue();

    @Modifying
    @Query("UPDATE CampaignEntity SET active = TRUE WHERE startPeriod >= :today AND active = FALSE AND endPeriod >= :today")
    List<CampaignEntity> updateCompaignsByActiveFalseAndStartPeriodEqualsOrAfterToday(@Param("today") LocalDate today);

    @Modifying
    @Query("UPDATE CampaignEntity SET active = FALSE WHERE endPeriod < :today AND active = TRUE ")
    void updateCompaignsByActiveTrueAndEndPeriodBeforeToday(@Param("today") LocalDate today);

    List<CampaignEntity> findByActiveTrueAndTeamId(Long teamId);
}
