package com.example.campaignapi.repository;

import com.example.campaignapi.domain.CampaignEntity;
import com.example.campaignapi.domain.UserCampaignEntity;
import com.example.campaignapi.domain.UserCampaignEntityKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCampaignRepository extends JpaRepository<UserCampaignEntity, UserCampaignEntityKey> {

    @Query("SELECT ca FROM UserCampaignEntity u " +
            " JOIN FETCH CampaignEntity ca on u.composeId.campaignId = ca.id " +
            " WHERE u.composeId.email = :email ")
    List<CampaignEntity> findByComposeIdEmail(@Param("email") String email);

    @Modifying
    @Query("DELETE UserCampaignEntity u WHERE u.composeId.campaignId = :campaignId ")
    void deleteByCampaignId(@Param("campaignId") Long campaignId);
}
