package com.example.campaignapi.controller;

import com.example.campaignapi.controller.request.CampaignCreationRequest;
import com.example.campaignapi.controller.request.CampaignUpdateRequest;
import com.example.campaignapi.controller.response.CampaignResponse;
import com.example.campaignapi.service.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("campaign")
public class CampaignController {

    @Autowired
    private CampaignService campaignService;

    @PostMapping
    public ResponseEntity<CampaignResponse> create(@Valid @RequestBody CampaignCreationRequest request){
        return new ResponseEntity(campaignService.create(request), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<CampaignResponse>> findActives(){
        return new ResponseEntity(campaignService.findAllActives(), HttpStatus.OK);
    }

    @GetMapping("/team/{teamId}")
    public ResponseEntity<List<CampaignResponse>> findActivesByTeam(@PathVariable Long teamId){
        return new ResponseEntity(campaignService.findActivesByTeam(teamId), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<CampaignResponse>> findById(@PathVariable Long id){
        return new ResponseEntity(campaignService.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        campaignService.delete(id);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CampaignResponse> update(@Valid @RequestBody CampaignUpdateRequest request, @PathVariable Long id){
        return new ResponseEntity(campaignService.update(request,id),HttpStatus.ACCEPTED);
    }

    @PostMapping("/{id}/relate/customer")
    public ResponseEntity<?> relate(@PathVariable Long id, @RequestParam("email") String email){
        campaignService.relateCustomer(id,email);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/find/customer")
    public ResponseEntity<List<CampaignResponse>> findCampaignByCustomer(@RequestParam("email") String email){
        return new ResponseEntity(campaignService.findCampaignByCustomer(email), HttpStatus.OK);
    }
}
