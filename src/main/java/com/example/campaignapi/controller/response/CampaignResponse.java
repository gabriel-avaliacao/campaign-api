package com.example.campaignapi.controller.response;

import com.example.campaignapi.utils.ConstatsValues;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CampaignResponse {

    private Long id;
    private String name;
    private Long teamId;
    private boolean active;

    @JsonFormat(pattern = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_FORMAT)
    private LocalDate startPeriod;

    @JsonFormat(pattern = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_FORMAT)
    private LocalDate endPeriod;

    @JsonFormat(pattern = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_FORMAT)
    private LocalDate createdDate;

    @JsonFormat(pattern = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_FORMAT)
    private LocalDate updateDate;

}
