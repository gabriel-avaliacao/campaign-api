package com.example.campaignapi.controller.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DefaultErrorResponse {

    private String code;
    private String description;
}
