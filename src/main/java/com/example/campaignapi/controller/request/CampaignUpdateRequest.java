package com.example.campaignapi.controller.request;

import com.example.campaignapi.utils.ConstatsValues;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CampaignUpdateRequest {

    private String name;
    private Long teamId;

    @ApiModelProperty(example = "00/00/0000")
    @Pattern(regexp = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX,
             message = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX_MESSAGE_ERROR)
    private String startPeriod;

    @ApiModelProperty(example = "00/00/0000")
    @Pattern(regexp = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX,
             message = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX_MESSAGE_ERROR)
    private String endPeriod;
}
