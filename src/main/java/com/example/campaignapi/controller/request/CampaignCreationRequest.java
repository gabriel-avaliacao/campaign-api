package com.example.campaignapi.controller.request;

import com.example.campaignapi.utils.ConstatsValues;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CampaignCreationRequest {

    @NotNull
    @NotBlank
    private String name;

    @NotNull
    private Long teamId;

    @NotNull
    @NotBlank
    @ApiModelProperty(example = "00/00/0000")
    @Pattern(regexp = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX,
             message = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX_MESSAGE_ERROR)
    private String startPeriod;

    @NotNull
    @NotBlank
    @ApiModelProperty(example = "00/00/0000")
    @Pattern(regexp = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX,
             message = ConstatsValues.PATTERN_CAMPAIGN_PERIOD_REGEX_MESSAGE_ERROR)
    private String endPeriod;
}
