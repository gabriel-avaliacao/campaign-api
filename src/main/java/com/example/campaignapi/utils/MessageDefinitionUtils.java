package com.example.campaignapi.utils;

import com.example.campaignapi.controller.response.DefaultErrorResponse;
import lombok.Getter;

@Getter
public enum MessageDefinitionUtils {

    ERROR_VALIDATE_DATE("100000","Verifique se os períodos informados estão corretos!"),
    ERROR_START_DATE_AFTER_END_DATE("100001","Período incial não pode ser maior que período final. "),
    ERROR_END_DATE_BEFORE_TODAY("100002","Período final não pode ser maior que a data atual. "),
    ERROR_CAMPAIGN_NOT_FOUND("100003","Campanha não encontrada."),
    ERROR_EMAIL_EMPTY_OR_NULL("100004","Email não foi informado."),

    ;
    private final String code;
    private final String description;

    MessageDefinitionUtils(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public DefaultErrorResponse getDefaultErrorResponse(){
        return DefaultErrorResponse.builder()
                .code(code)
                .description(description)
                .build();
    }

}
