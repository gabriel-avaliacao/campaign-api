package com.example.campaignapi;

import com.example.campaignapi.domain.CampaignEntity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CampaignMessageDTO {

    private Long campaignId;
    private String status;
}
