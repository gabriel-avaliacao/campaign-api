package com.example.campaignapi.schedule;

import com.example.campaignapi.service.CampaignService;
import com.example.campaignapi.utils.ValidatePeriodUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Slf4j
@Component
public class DisableExpiredCampaignSchedule {

    @Autowired
    private CampaignService campaignService;

    @Scheduled(cron = "1 0 0 * * *")
    public void disabledExpiredCampaign(){
        log.info("M=disabledExpiredCampaign, status=start, iniciando schedule para desabilitar campanhas inválidas.");
        campaignService.disabledExpiredCampaign();
        log.info("M=disabledExpiredCampaign, status=end");
    }

    @Scheduled(cron = "0 1 0 * * *")
    public void activeCampaigns(){
        log.info("M=activeCampaigns, status=start, iniciando schedule para habilitar campanhas que possuem data de vigência" +
                "a partir de hoje={}.", LocalDate.now().format(ValidatePeriodUtils.DATE_FORMAT));
        campaignService.activeCampaigns();
        log.info("M=activeCampaigns, status=end");
    }

}
