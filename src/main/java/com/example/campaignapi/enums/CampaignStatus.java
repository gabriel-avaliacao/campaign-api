package com.example.campaignapi.enums;

public enum CampaignStatus {

    CREATED,
    UPDATED,
    DELETED;
}
