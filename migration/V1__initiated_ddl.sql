CREATE TABLE campaign (
	idt_campaign BIGINT NOT NULL AUTO_INCREMENT,
	des_name VARCHAR(150) NOT NULL,
	idt_team BIGINT NOT NULL,
	dat_start_period DATE NOT NULL,
	dat_end_period DATE NOT NULL,
	dat_created DATETIME NOT NULL DEFAULT NOW(),
	dat_update DATETIME NULL,
	flg_active BOOLEAN NOT NULL,
	PRIMARY KEY (idt_campaign)
);

CREATE TABLE user_campaign (
	idt_campaign BIGINT NOT NULL,
	des_user_email VARCHAR(150) NOT NULL,
	PRIMARY KEY (idt_campaign,des_user_email),
	FOREIGN KEY (idt_campaign) REFERENCES campaign (idt_campaign)
);